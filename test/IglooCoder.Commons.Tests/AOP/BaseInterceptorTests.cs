using System;
using Castle.DynamicProxy;
using IglooCoder.Commons.AOP;
using NUnit.Framework;
using Rhino.Mocks;

namespace IglooCoder.Commons.Tests.AOP
{
    [TestFixture]
    public class When_intercepting_an_invocation_that_throws_an_exception
    {
        private BaseInterceptorHelper _baseInterceptorHelper;
        
        [SetUp]
        public void Setup()
        {
            var invocation = MockRepository.GenerateStub<IInvocation>();
            invocation.Stub(x => x.Proceed()).Throw(new Exception());
            _baseInterceptorHelper = new BaseInterceptorHelper();
            try
            {
                _baseInterceptorHelper.Intercept(invocation);
            }
            catch
            {       
            }
        }

        [Test]
        public void the_onexception_code_should_be_executed()
        {
            Assert.That(_baseInterceptorHelper.OnExceptionCalled, Is.True);
        }

        [Test]
        public void the_onexit_code_should_be_executed()
        {
            Assert.That(_baseInterceptorHelper.OnExitCalled, Is.True);    
        }
    }

    [TestFixture]
    public class When_intercepting_an_invocation
    {
        private BaseInterceptorHelper _baseInterceptorHelper;

        [SetUp]
        public void Setup()
        {
            var invocation = MockRepository.GenerateStub<IInvocation>();
            _baseInterceptorHelper = new BaseInterceptorHelper();

            _baseInterceptorHelper.Intercept(invocation);
            
        }

        [Test]
        public void the_onentry_code_should_be_executed()
        {
            Assert.That(_baseInterceptorHelper.OnEnterCalled, Is.True);
        }

        [Test]
        public void the_onsuccess_code_should_be_executed()
        {
            Assert.That(_baseInterceptorHelper.OnSuccessCalled, Is.True);
        }

        [Test]
        public void the_onexit_code_should_be_executed()
        {
            Assert.That(_baseInterceptorHelper.OnExitCalled, Is.True);
        } 
    }


    public class BaseInterceptorHelper:BaseInterceptor
    {
        public bool OnEnterCalled { get; private set; }
        public bool OnExceptionCalled { get; private set; }
        public bool OnExitCalled { get; private set; }
        public bool OnSuccessCalled { get; private set; }

        public override void OnEnter(IInvocation invocation)
        {
            OnEnterCalled = true;
        }

        public override void OnSuccess(IInvocation invocation)
        {
            OnSuccessCalled = true;
        }

        public override void OnException(IInvocation invocation, Exception exception)
        {
            OnExceptionCalled = true;
        }

        public override void OnExit(IInvocation invocation)
        {
            OnExitCalled = true;
        }
    }
}