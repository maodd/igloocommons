using System;
using IglooCoder.Commons.Logging;
using NUnit.Framework;
using Rhino.Mocks;

namespace IglooCoder.Commons.Tests.Logging
{
    public class LoggingTests
    {
        [TestFixture]
        public class When_logger_is_initialized
        {
            [SetUp]
            public void Setup()
            {
                Log.Initialize(new HelperLogger());
            }

            [Test]
            public void a_logging_context_should_be_available_for_use()
            {
                Assert.That(Log.For(this), Is.InstanceOf(typeof (ILoggingContext)));
            }
        }

        [TestFixture]
        public class When_logging_through_a_context
        {
            private ILoggingContext _loggingContextMock;

            [SetUp]
            public void Setup()
            {
                _loggingContextMock = MockRepository.GenerateMock<ILoggingContext>();
                var loggerMock = MockRepository.GenerateStub<ILogger>();
                loggerMock.Stub(x => x.ContextFor(Arg<object>.Is.Anything)).Return(_loggingContextMock);

                Log.Initialize(loggerMock);
            }

            [Test]
            public void the_underlying_logger_should_be_called_during_an_error_entry()
            {
                Log.For(this).Error(string.Empty, new Exception());
                _loggingContextMock.AssertWasCalled(x=>x.Error(Arg<string>.Is.Anything, Arg<Exception>.Is.Anything));
            }

            [Test]
            public void the_underlying_logger_should_be_called_during_a_debug_entry()
            {
                Log.For(this).Debug(string.Empty);
                _loggingContextMock.AssertWasCalled(x=>x.Debug(Arg<string>.Is.Anything));
            }

            [Test]
            public void the_underlying_logger_should_be_called_during_an_info_entry()
            {
                Log.For(this).Info(string.Empty);
                _loggingContextMock.AssertWasCalled(x=>x.Info(Arg<string>.Is.Anything));
            }
        }
    }
}