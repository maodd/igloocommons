using IglooCoder.Commons.Logging;

namespace IglooCoder.Commons.Tests.Logging
{
    public class HelperLogger : ILogger
    {
        public ILoggingContext ContextFor(object objectToLogFor)
        {
            return new HelperContext();
        }
    }
}