using System.IO;
using IglooCoder.Commons.Logging;
using NUnit.Framework;

namespace IglooCoder.Commons.Tests.Logging
{
    public class Log4NetTests
    {
        [TestFixture]
        public class When_configuring_a_logger_with_only_a_configuration_file_name_and_logging_a_value
        {
            private string _logFileName;

            [SetUp]
            public void Setup()
            {
                _logFileName = "Log4NetLoggerTests.log";
                Log.Initialize(new Log4NetLogger("log4net.config"));
                Log.For(this).Debug("testing message");
            }

            [Test]
            public void a_file_should_be_created()
            {
                Assert.That(File.Exists(_logFileName), Is.True);
            }
        }

        [TestFixture]
        public class When_logging_an_entry
        {
            private string _logFileName;

            [SetUp]
            public void Setup()
            {
                _logFileName = "Log4NetLoggerTests.log";
                log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));
                Log.Initialize(new Log4NetLogger(new Log4NetContext(log4net.LogManager.GetLogger(this.GetType()))));
                Log.For(this).Debug("some test message");
            }

            [Test]
            public void a_log_file_should_be_created()
            {
                Assert.That(File.Exists(_logFileName), Is.True);
            }
        }
    }
}