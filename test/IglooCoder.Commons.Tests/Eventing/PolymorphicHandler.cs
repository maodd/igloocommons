﻿using IglooCoder.Commons.Eventing;

namespace IglooCoder.Commons.Tests.Eventing
{
    public class PolymorphicHandler : IHandleMessagesOfType<PolymorphicTestMessage>,
                                      IHandleMessagesOfType<PolymorphicBaseTestMessage>
    {
        public void Handle(PolymorphicTestMessage message)
        {
            PolymorphicTestMessageHandled = true;
        }

        public bool PolymorphicTestMessageHandled { get; set; }

        public void Handle(PolymorphicBaseTestMessage message)
        {
            PolymorphicBaseTestMessageHandled = true;
        }

        public bool PolymorphicBaseTestMessageHandled { get; set; }
    }
}