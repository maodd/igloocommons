using IglooCoder.Commons.Eventing;
using NUnit.Framework;

namespace IglooCoder.Commons.Tests.Eventing
{
    public class EventAggregatorTests
    {
        [TestFixture]
        public class When_submitting_a_structured_interface
        {
            private IEventAggregator _eventAggregator;
            private InterfaceScenarioHelper _handler;

            [SetUp]
            public void Setup()
            {
                _handler = new InterfaceScenarioHelper();
                _eventAggregator = new EventAggregator();

                _eventAggregator.Register(_handler);

                _eventAggregator.Raise<IStructuredMessage>(x =>
                                                               {
                                                                   x.Property1 = "asdf";
                                                                   x.Property2 = 123;
                                                               });
            }

            [Test]
            public void the_event_should_be_handled_with_a_data_filled_message()
            {
                Assert.That(_handler.DataFilledMessageProperty1, Is.EqualTo("asdf"));
                Assert.That(_handler.DataFilledMessageProperty2, Is.EqualTo(123));
            }
        }

        [TestFixture]
        public class When_submitting_an_empty_interface
        {
            private IEventAggregator _eventAggregator;
            private InterfaceScenarioHelper _handler;

            [SetUp]
            public void Setup()
            {
                _handler = new InterfaceScenarioHelper();

                _eventAggregator = new EventAggregator();
                _eventAggregator.Register(_handler);

                _eventAggregator.Raise<IEmptyMessage>();
            }

            [Test]
            public void the_event_should_be_raised_with_a_non_null_message()
            {
                Assert.That(_handler.NonNullMessageHandled, Is.True);
            }
        }

        [TestFixture]
        public class When_submitting_a_super_class
        {
            private IEventAggregator _eventAggregator;
            private BaseHandler _baseHandler;

            [SetUp]
            public void Setup()
            {
                _baseHandler = new BaseHandler();

                _eventAggregator = new EventAggregator();

                _eventAggregator.Register(_baseHandler);

                _eventAggregator.Raise<PolymorphicTestMessage>();
            }

            [Test]
            public void the_base_objects_handler_should_be_executed()
            {
                Assert.That(_baseHandler.MessageHandled, Is.True);
            }
        }

        [TestFixture]
        public class When_unregistering_an_item
        {
            private IEventAggregator _eventAggregator;
            private PolymorphicHandler _handler;

            [SetUp]
            public void Setup()
            {
                _handler = new PolymorphicHandler();

                _eventAggregator = new EventAggregator();

                _eventAggregator.Register(_handler);

                _eventAggregator.Unregister(_handler);

                _eventAggregator.Raise<PolymorphicTestMessage>();
            }

            [Test]
            public void it_should_no_longer_handle_messages()
            {
                Assert.That(_handler.PolymorphicTestMessageHandled, Is.False);
            }
        }

        [TestFixture]
        public class When_submitting_a_polymorphic_message
        {
            private IEventAggregator _eventAggregator;
            private PolymorphicHandler _polymorpicHandlerClass;

            [SetUp]
            public void Setup()
            {
                _polymorpicHandlerClass = new PolymorphicHandler();

                _eventAggregator = new EventAggregator();

                _eventAggregator.Register(_polymorpicHandlerClass);

                _eventAggregator.Raise(new PolymorphicTestMessage());
            }

            [Test]
            public void the_message_should_be_handled_by_all_appropriate_handlers()
            {
                Assert.That(_polymorpicHandlerClass.PolymorphicTestMessageHandled, Is.True);
                Assert.That(_polymorpicHandlerClass.PolymorphicBaseTestMessageHandled, Is.True);
            }
        }

        [TestFixture]
        public class When_submitting_an_event_that_is_handled_by_a_registered_object
        {
            private IEventAggregator _eventAggregator;
            private EventingHelperClass _eventingHelperClass;
            private SecondHelperClass _secondHelperClass;

            [SetUp]
            public void Setup()
            {
                _eventingHelperClass = new EventingHelperClass();
                _secondHelperClass = new SecondHelperClass();

                _eventAggregator = new EventAggregator();
                _eventAggregator.Register(_eventingHelperClass);
                _eventAggregator.Register(_secondHelperClass);

                _eventAggregator.Raise<TestingMessage>();
            }

            [Test]
            public void the_registered_object_should_handle_the_message()
            {
                Assert.That(_eventingHelperClass.EventWasHandled, Is.True);
            }

            [Test]
            public void all_registered_objects_should_handle_the_message()
            {
                Assert.That(_eventingHelperClass.EventWasHandled, Is.True);
                Assert.That(_secondHelperClass.EventWasHandled, Is.True);
            }
        }
    }
}