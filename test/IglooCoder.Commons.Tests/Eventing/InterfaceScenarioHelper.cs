﻿using System;
using IglooCoder.Commons.Eventing;

namespace IglooCoder.Commons.Tests.Eventing
{
    public class InterfaceScenarioHelper : IHandleMessagesOfType<IEmptyMessage>, IHandleMessagesOfType<IStructuredMessage>
    {
        public void Handle(IEmptyMessage message)
        {
            if (message != null)
            {
                NonNullMessageHandled = true;
            }
        }

        public bool NonNullMessageHandled { get; set; }

        public string DataFilledMessageProperty1 { get; set; }

        public int DataFilledMessageProperty2 { get; set; }

        public void Handle(IStructuredMessage message)
        {
            DataFilledMessageProperty1 = message.Property1;
            DataFilledMessageProperty2 = message.Property2;
        }
    }
}