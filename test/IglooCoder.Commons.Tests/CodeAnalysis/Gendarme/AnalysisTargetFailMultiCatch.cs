﻿using System;

namespace IglooCoder.Commons.Tests.CodeAnalysis.Gendarme
{
    public class AnalysisTargetFailMultiCatch
    {

        public void DoSomethingWrongTwice()
        {
            try
            {
                var i = 1;
            }
            catch (ApplicationException)
            {
                var y = 1;
            }
            catch (Exception)
            {
                var x = 1;
            }
        }
    }
}