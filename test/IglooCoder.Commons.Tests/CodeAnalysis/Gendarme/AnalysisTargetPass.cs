﻿using System;

namespace IglooCoder.Commons.Tests.CodeAnalysis.Gendarme
{
    public class AnalysisTargetPass
    {
        public void DoSomethingProper()
        {
            try
            {
                var x = 2;
            }
            catch (Exception)
            {
                var i = 1;
                throw;
            }
        }


    }
}