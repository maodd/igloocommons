using System;
using Castle.DynamicProxy;
using IglooCoder.Commons.Logging;

namespace IglooCoder.Commons.AOP
{
    public class LoggingInterceptor : BaseInterceptor
    {
        private readonly IParseInputParameters _parameterParser;

        public LoggingInterceptor(IParseInputParameters parameterParser)
        {
            _parameterParser = parameterParser;
        }

        public override void OnEnter(IInvocation invocation)
        {
            Log.For(invocation.TargetType)
                .Info(string.Format("Input: {0}",
                                    _parameterParser.On(invocation.Arguments)));
        }

        public override void OnSuccess(IInvocation invocation)
        {
            Log.For(invocation.TargetType)
                .Info(string.Format("Returned: {0}",
                                    _parameterParser.On(new object[1] {invocation.ReturnValue})));
        }

        public override void OnException(IInvocation invocation, Exception exception)
        {
            Log.For(invocation.TargetType)
                .Error(string.Format("An exception occurred in {0}.{1}", invocation.Method.Module,
                                     invocation.Method.Name), exception);
        }

        public override void OnExit(IInvocation invocation)
        {
            //do nothing
        }
    }
}