using System;
using Castle.DynamicProxy;

namespace IglooCoder.Commons.AOP
{
    public abstract class BaseOnEntryInterceptor : BaseInterceptor
    {
        public override void OnException(IInvocation invocation, Exception exception)
        {
            //do nothing
        }

        public override void OnExit(IInvocation invocation)
        {
            //do nothing
        }

        public override void OnSuccess(IInvocation invocation)
        {
            //do nothing
        }
    }
}