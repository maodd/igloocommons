namespace IglooCoder.Commons.AOP
{
    public interface IParseInputParameters
    {
        string On(object[] parametersToBeParsed);
    }
}