using System;

namespace IglooCoder.Commons.AOP
{
    public interface ILoggingParseTypeValidator
    {
        bool ShouldParseInDetail(Type typeBeingParsed);
    }
}