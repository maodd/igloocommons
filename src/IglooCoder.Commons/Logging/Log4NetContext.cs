using System;

namespace IglooCoder.Commons.Logging
{
    public class Log4NetContext : ILoggingContext
    {
        private log4net.ILog _log;

        public Log4NetContext(log4net.ILog log)
        {
            _log = log;
        }

        public void Error(string messageToLog, Exception underlyingErrorException)
        {
            _log.Error(messageToLog, underlyingErrorException);
        }

        public void Debug(string messageToLog)
        {
            _log.Debug(messageToLog);
        }

        public void Info(string messageToLog)
        {
            _log.Info(messageToLog);
        }
    }
}