namespace IglooCoder.Commons.Logging
{
    public class NullLogger:ILogger
    {
        public ILoggingContext ContextFor(object objectToLogFor)
        {
            return new NullLoggingContext();
        }
    }
}