using System;

namespace IglooCoder.Commons.Logging
{
    public class NullLoggingContext:ILoggingContext
    {
        public void Error(string messageToLog, Exception underlyingErrorException)
        {}

        public void Debug(string messageToLog)
        {}

        public void Info(string messageToLog)
        {}
    }
}