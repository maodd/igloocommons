using NHibernate;
using NHibernate.Cfg;

namespace IglooCoder.Commons.WcfNhibernate
{
    public static class NHibernateFactory
    {
        private static ISessionFactory _sessionFactory;

        public static void Initialize()
        {
            Initialize(new Configuration().Configure().BuildSessionFactory());
        }

        public static void Initialize(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public static ISession OpenSession()
        {
            return _sessionFactory.OpenSession();
        }
    }
}