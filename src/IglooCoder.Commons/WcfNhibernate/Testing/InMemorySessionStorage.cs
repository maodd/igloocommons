using NHibernate;

namespace IglooCoder.Commons.WcfNhibernate.Testing
{
    public class InMemorySessionStorage:ISessionStorage
    {
        private readonly ISession _session;

        public InMemorySessionStorage(ISession session)
        {
            _session = session;
        }

        public ISession GetSession()
        {
            return _session;
        }
    }
}