namespace IglooCoder.Commons.WcfNhibernate
{
    public interface IDomain
    {
        long Id { get; set; }
    }
}