namespace IglooCoder.Commons.Specification
{
    public class AndSpecification<T> : CompositeSpecification<T>
    {
        private readonly ISpecification<T> _firstSpecification;
        private readonly ISpecification<T> _secondSpecification;

        public AndSpecification(ISpecification<T> firstSpecification, ISpecification<T> secondSpecification)
        {
            _firstSpecification = firstSpecification;
            _secondSpecification = secondSpecification;
        }

        public override bool IsSatisfiedBy(T itemToVerify)
        {
            return _firstSpecification.IsSatisfiedBy(itemToVerify) &&
                   _secondSpecification.IsSatisfiedBy(itemToVerify);
        }

    }
}