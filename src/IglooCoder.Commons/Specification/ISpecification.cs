namespace IglooCoder.Commons.Specification
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T itemToVerify);
        ISpecification<T> And(ISpecification<T> specification);
        ISpecification<T> Or(ISpecification<T> specification);
    }
}